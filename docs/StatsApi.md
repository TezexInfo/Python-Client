# tezex_client.StatsApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_statistics**](StatsApi.md#get_statistics) | **GET** /stats/{group}/{stat}/{period} | Get Statistics
[**get_stats_overview**](StatsApi.md#get_stats_overview) | **GET** /stats/overview | Returns some basic Info


# **get_statistics**
> list[Stats] get_statistics(group, stat, period, start_time=start_time, end_time=end_time)

Get Statistics

Get Statistics

### Example 
```python
import time
import tezex_client
from tezex_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = tezex_client.StatsApi()
group = 'group_example' # str | Block, Transaction, etc
stat = 'stat_example' # str | 
period = 'period_example' # str | 
start_time = '2013-10-20T19:20:30+01:00' # datetime |  (optional)
end_time = '2013-10-20T19:20:30+01:00' # datetime |  (optional)

try: 
    # Get Statistics
    api_response = api_instance.get_statistics(group, stat, period, start_time=start_time, end_time=end_time)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling StatsApi->get_statistics: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group** | **str**| Block, Transaction, etc | 
 **stat** | **str**|  | 
 **period** | **str**|  | 
 **start_time** | **datetime**|  | [optional] 
 **end_time** | **datetime**|  | [optional] 

### Return type

[**list[Stats]**](Stats.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_stats_overview**
> StatsOverview get_stats_overview()

Returns some basic Info

### Example 
```python
import time
import tezex_client
from tezex_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = tezex_client.StatsApi()

try: 
    # Returns some basic Info
    api_response = api_instance.get_stats_overview()
    pprint(api_response)
except ApiException as e:
    print "Exception when calling StatsApi->get_stats_overview: %s\n" % e
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**StatsOverview**](StatsOverview.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

