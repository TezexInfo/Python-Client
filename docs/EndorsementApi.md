# tezex_client.EndorsementApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_endorsement**](EndorsementApi.md#get_endorsement) | **GET** /endorsement/{endorsement_hash} | Get Endorsement
[**get_endorsement_for_block**](EndorsementApi.md#get_endorsement_for_block) | **GET** /endorsement/for/{block_hash} | Get Endorsement


# **get_endorsement**
> Endorsement get_endorsement(endorsement_hash)

Get Endorsement

Get a specific Endorsement

### Example 
```python
import time
import tezex_client
from tezex_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = tezex_client.EndorsementApi()
endorsement_hash = 'endorsement_hash_example' # str | The hash of the Endorsement to retrieve

try: 
    # Get Endorsement
    api_response = api_instance.get_endorsement(endorsement_hash)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling EndorsementApi->get_endorsement: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **endorsement_hash** | **str**| The hash of the Endorsement to retrieve | 

### Return type

[**Endorsement**](Endorsement.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_endorsement_for_block**
> list[Endorsement] get_endorsement_for_block(block_hash)

Get Endorsement

Get a specific Endorsement

### Example 
```python
import time
import tezex_client
from tezex_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = tezex_client.EndorsementApi()
block_hash = 'block_hash_example' # str | blockhash

try: 
    # Get Endorsement
    api_response = api_instance.get_endorsement_for_block(block_hash)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling EndorsementApi->get_endorsement_for_block: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **block_hash** | **str**| blockhash | 

### Return type

[**list[Endorsement]**](Endorsement.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

