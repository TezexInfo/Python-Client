# tezex_client.MarketApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**candlestick**](MarketApi.md#candlestick) | **GET** /price/{denominator}/{numerator}/{period} | Candlestick Data
[**ticker**](MarketApi.md#ticker) | **GET** /ticker/{numerator} | Get Ticker for a specific Currency


# **candlestick**
> list[Candlestick] candlestick(denominator, numerator, period)

Candlestick Data

Returns CandleStick Prices

### Example 
```python
import time
import tezex_client
from tezex_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = tezex_client.MarketApi()
denominator = 'denominator_example' # str | which currency
numerator = 'numerator_example' # str | to which currency
period = 'period_example' # str | Timeframe of one candle

try: 
    # Candlestick Data
    api_response = api_instance.candlestick(denominator, numerator, period)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling MarketApi->candlestick: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **denominator** | **str**| which currency | 
 **numerator** | **str**| to which currency | 
 **period** | **str**| Timeframe of one candle | 

### Return type

[**list[Candlestick]**](Candlestick.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ticker**
> Ticker ticker(numerator)

Get Ticker for a specific Currency

Returns BTC, USD, EUR and CNY Prices

### Example 
```python
import time
import tezex_client
from tezex_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = tezex_client.MarketApi()
numerator = 'numerator_example' # str | The level of the Blocks to retrieve

try: 
    # Get Ticker for a specific Currency
    api_response = api_instance.ticker(numerator)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling MarketApi->ticker: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **numerator** | **str**| The level of the Blocks to retrieve | 

### Return type

[**Ticker**](Ticker.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

