# Candlestick

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**o** | **str** |  | [optional] 
**h** | **str** |  | [optional] 
**l** | **str** |  | [optional] 
**c** | **str** |  | [optional] 
**vol** | **str** |  | [optional] 
**time** | **datetime** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


