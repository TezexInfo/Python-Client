# tezex_client.BlockApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**blocks_all**](BlockApi.md#blocks_all) | **GET** /blocks/all | Get All Blocks 
[**blocks_by_level**](BlockApi.md#blocks_by_level) | **GET** /blocks/{level} | Get All Blocks for a specific Level
[**blocks_by_level_range**](BlockApi.md#blocks_by_level_range) | **GET** /blocks/{startlevel}/{stoplevel} | Get All Blocks for a specific Level-Range
[**get_block**](BlockApi.md#get_block) | **GET** /block/{blockhash} | Get Block By Blockhash
[**get_block_delegations**](BlockApi.md#get_block_delegations) | **GET** /block/{blockhash}/operations/delegations | Get Delegations of a Block
[**get_block_endorsements**](BlockApi.md#get_block_endorsements) | **GET** /block/{blockhash}/operations/endorsements | Get Endorsements of a Block
[**get_block_operations_sorted**](BlockApi.md#get_block_operations_sorted) | **GET** /block/{blockhash}/operations | Get operations of a block, sorted
[**get_block_originations**](BlockApi.md#get_block_originations) | **GET** /block/{blockhash}/operations/originations | Get Originations of a Block
[**get_block_transaction**](BlockApi.md#get_block_transaction) | **GET** /block/{blockhash}/operations/transactions | Get Transactions of Block
[**recent_blocks**](BlockApi.md#recent_blocks) | **GET** /blocks/recent | returns the last 25 blocks


# **blocks_all**
> BlocksAll blocks_all(page=page, order=order, limit=limit)

Get All Blocks 

Get all Blocks

### Example 
```python
import time
import tezex_client
from tezex_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = tezex_client.BlockApi()
page = 3.4 # float | Pagination, 200 tx per page max (optional)
order = 'order_example' # str | ASC or DESC (optional)
limit = 56 # int | Results per Page (optional)

try: 
    # Get All Blocks 
    api_response = api_instance.blocks_all(page=page, order=order, limit=limit)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling BlockApi->blocks_all: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **float**| Pagination, 200 tx per page max | [optional] 
 **order** | **str**| ASC or DESC | [optional] 
 **limit** | **int**| Results per Page | [optional] 

### Return type

[**BlocksAll**](BlocksAll.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **blocks_by_level**
> list[Block] blocks_by_level(level)

Get All Blocks for a specific Level

Get all Blocks for a specific Level

### Example 
```python
import time
import tezex_client
from tezex_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = tezex_client.BlockApi()
level = 3.4 # float | The level of the Blocks to retrieve, includes abandoned

try: 
    # Get All Blocks for a specific Level
    api_response = api_instance.blocks_by_level(level)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling BlockApi->blocks_by_level: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **level** | **float**| The level of the Blocks to retrieve, includes abandoned | 

### Return type

[**list[Block]**](Block.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **blocks_by_level_range**
> BlockRange blocks_by_level_range(startlevel, stoplevel)

Get All Blocks for a specific Level-Range

Get all Blocks for a specific Level-Range

### Example 
```python
import time
import tezex_client
from tezex_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = tezex_client.BlockApi()
startlevel = 3.4 # float | lowest blocklevel to return
stoplevel = 3.4 # float | highest blocklevel to return

try: 
    # Get All Blocks for a specific Level-Range
    api_response = api_instance.blocks_by_level_range(startlevel, stoplevel)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling BlockApi->blocks_by_level_range: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **startlevel** | **float**| lowest blocklevel to return | 
 **stoplevel** | **float**| highest blocklevel to return | 

### Return type

[**BlockRange**](BlockRange.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_block**
> Block get_block(blockhash)

Get Block By Blockhash

Get a block by its hash

### Example 
```python
import time
import tezex_client
from tezex_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = tezex_client.BlockApi()
blockhash = 'blockhash_example' # str | The hash of the Block to retrieve

try: 
    # Get Block By Blockhash
    api_response = api_instance.get_block(blockhash)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling BlockApi->get_block: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blockhash** | **str**| The hash of the Block to retrieve | 

### Return type

[**Block**](Block.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_block_delegations**
> list[Delegation] get_block_delegations(blockhash)

Get Delegations of a Block

Get all Delegations of a specific Block

### Example 
```python
import time
import tezex_client
from tezex_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = tezex_client.BlockApi()
blockhash = 'blockhash_example' # str | Blockhash

try: 
    # Get Delegations of a Block
    api_response = api_instance.get_block_delegations(blockhash)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling BlockApi->get_block_delegations: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blockhash** | **str**| Blockhash | 

### Return type

[**list[Delegation]**](Delegation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_block_endorsements**
> list[Endorsement] get_block_endorsements(blockhash)

Get Endorsements of a Block

Get all Endorsements of a specific Block

### Example 
```python
import time
import tezex_client
from tezex_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = tezex_client.BlockApi()
blockhash = 'blockhash_example' # str | Blockhash

try: 
    # Get Endorsements of a Block
    api_response = api_instance.get_block_endorsements(blockhash)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling BlockApi->get_block_endorsements: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blockhash** | **str**| Blockhash | 

### Return type

[**list[Endorsement]**](Endorsement.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_block_operations_sorted**
> BlockOperationsSorted get_block_operations_sorted(blockhash)

Get operations of a block, sorted

Get the maximum Level we have seen, Blocks at this level may become abandoned Blocks later on

### Example 
```python
import time
import tezex_client
from tezex_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = tezex_client.BlockApi()
blockhash = 'blockhash_example' # str | The hash of the Block to retrieve

try: 
    # Get operations of a block, sorted
    api_response = api_instance.get_block_operations_sorted(blockhash)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling BlockApi->get_block_operations_sorted: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blockhash** | **str**| The hash of the Block to retrieve | 

### Return type

[**BlockOperationsSorted**](BlockOperationsSorted.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_block_originations**
> list[Origination] get_block_originations(blockhash)

Get Originations of a Block

Get all Originations of a spcific Block

### Example 
```python
import time
import tezex_client
from tezex_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = tezex_client.BlockApi()
blockhash = 'blockhash_example' # str | Blockhash

try: 
    # Get Originations of a Block
    api_response = api_instance.get_block_originations(blockhash)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling BlockApi->get_block_originations: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blockhash** | **str**| Blockhash | 

### Return type

[**list[Origination]**](Origination.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_block_transaction**
> list[Transaction] get_block_transaction(blockhash)

Get Transactions of Block

Get all Transactions of a spcific Block

### Example 
```python
import time
import tezex_client
from tezex_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = tezex_client.BlockApi()
blockhash = 'blockhash_example' # str | Blockhash

try: 
    # Get Transactions of Block
    api_response = api_instance.get_block_transaction(blockhash)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling BlockApi->get_block_transaction: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blockhash** | **str**| Blockhash | 

### Return type

[**list[Transaction]**](Transaction.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **recent_blocks**
> list[Block] recent_blocks()

returns the last 25 blocks

Get all Blocks for a specific Level

### Example 
```python
import time
import tezex_client
from tezex_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = tezex_client.BlockApi()

try: 
    # returns the last 25 blocks
    api_response = api_instance.recent_blocks()
    pprint(api_response)
except ApiException as e:
    print "Exception when calling BlockApi->recent_blocks: %s\n" % e
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[Block]**](Block.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

