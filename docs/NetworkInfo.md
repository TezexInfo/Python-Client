# NetworkInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**max_level** | **int** |  | [optional] 
**blocktime** | **str** |  | [optional] 
**transactions_24h** | **int** |  | [optional] 
**oeprations_24h** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


