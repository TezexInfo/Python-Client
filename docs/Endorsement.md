# Endorsement

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hash** | **str** |  | [optional] 
**source** | **str** |  | [optional] 
**level** | **int** |  | [optional] 
**block** | **str** |  | [optional] 
**endorsed_block** | **str** |  | [optional] 
**slot** | **int** |  | [optional] 
**time** | **datetime** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


