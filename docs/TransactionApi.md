# tezex_client.TransactionApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_transaction**](TransactionApi.md#get_transaction) | **GET** /transaction/{transaction_hash} | Get Transaction
[**get_transactions_recent**](TransactionApi.md#get_transactions_recent) | **GET** /transactions/recent | Returns the last 50 Transactions
[**transactions_all**](TransactionApi.md#transactions_all) | **GET** /transactions/all | Get All Transactions
[**transactions_by_level_range**](TransactionApi.md#transactions_by_level_range) | **GET** /transactions/{startlevel}/{stoplevel} | Get All Transactions for a specific Level-Range


# **get_transaction**
> Transaction get_transaction(transaction_hash)

Get Transaction

Get a specific Transaction

### Example 
```python
import time
import tezex_client
from tezex_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = tezex_client.TransactionApi()
transaction_hash = 'transaction_hash_example' # str | The hash of the Transaction to retrieve

try: 
    # Get Transaction
    api_response = api_instance.get_transaction(transaction_hash)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling TransactionApi->get_transaction: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **transaction_hash** | **str**| The hash of the Transaction to retrieve | 

### Return type

[**Transaction**](Transaction.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_transactions_recent**
> list[Transaction] get_transactions_recent()

Returns the last 50 Transactions

### Example 
```python
import time
import tezex_client
from tezex_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = tezex_client.TransactionApi()

try: 
    # Returns the last 50 Transactions
    api_response = api_instance.get_transactions_recent()
    pprint(api_response)
except ApiException as e:
    print "Exception when calling TransactionApi->get_transactions_recent: %s\n" % e
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[Transaction]**](Transaction.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **transactions_all**
> TransactionRange transactions_all(page=page, order=order, limit=limit)

Get All Transactions

Get all Transactions

### Example 
```python
import time
import tezex_client
from tezex_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = tezex_client.TransactionApi()
page = 3.4 # float | Pagination, 200 tx per page max (optional)
order = 'order_example' # str | ASC or DESC (optional)
limit = 56 # int | Results per Page (optional)

try: 
    # Get All Transactions
    api_response = api_instance.transactions_all(page=page, order=order, limit=limit)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling TransactionApi->transactions_all: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **float**| Pagination, 200 tx per page max | [optional] 
 **order** | **str**| ASC or DESC | [optional] 
 **limit** | **int**| Results per Page | [optional] 

### Return type

[**TransactionRange**](TransactionRange.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **transactions_by_level_range**
> TransactionRange transactions_by_level_range(startlevel, stoplevel, page=page, order=order, limit=limit)

Get All Transactions for a specific Level-Range

Get all Transactions for a specific Level-Range

### Example 
```python
import time
import tezex_client
from tezex_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = tezex_client.TransactionApi()
startlevel = 3.4 # float | lowest blocklevel to return
stoplevel = 3.4 # float | highest blocklevel to return
page = 3.4 # float | Pagination, 200 tx per page max (optional)
order = 'order_example' # str | ASC or DESC (optional)
limit = 56 # int | Results per Page (optional)

try: 
    # Get All Transactions for a specific Level-Range
    api_response = api_instance.transactions_by_level_range(startlevel, stoplevel, page=page, order=order, limit=limit)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling TransactionApi->transactions_by_level_range: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **startlevel** | **float**| lowest blocklevel to return | 
 **stoplevel** | **float**| highest blocklevel to return | 
 **page** | **float**| Pagination, 200 tx per page max | [optional] 
 **order** | **str**| ASC or DESC | [optional] 
 **limit** | **int**| Results per Page | [optional] 

### Return type

[**TransactionRange**](TransactionRange.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

