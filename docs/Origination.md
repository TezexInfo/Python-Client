# Origination

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hash** | **str** |  | [optional] 
**branch** | **str** |  | [optional] 
**source** | **str** |  | [optional] 
**public_key** | **str** |  | [optional] 
**fee** | **int** |  | [optional] 
**counter** | **int** |  | [optional] 
**operations** | [**list[OriginationOperation]**](OriginationOperation.md) |  | [optional] 
**level** | **int** |  | [optional] 
**block_hash** | **str** |  | [optional] 
**time** | **datetime** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


