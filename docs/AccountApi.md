# tezex_client.AccountApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_account**](AccountApi.md#get_account) | **GET** /account/{account} | Get Account
[**get_account_balance**](AccountApi.md#get_account_balance) | **GET** /account/{account}/balance | Get Account Balance
[**get_account_last_seen**](AccountApi.md#get_account_last_seen) | **GET** /account/{account}/last_seen | Get last active date
[**get_account_operation_count**](AccountApi.md#get_account_operation_count) | **GET** /account/{account}/operations_count | Get operation count of Account
[**get_account_transaction_count**](AccountApi.md#get_account_transaction_count) | **GET** /account/{account}/transaction_count | Get transaction count of Account
[**get_delegations_for_account**](AccountApi.md#get_delegations_for_account) | **GET** /account/{account}/delegations | Get Delegations of this account
[**get_delegations_to_account**](AccountApi.md#get_delegations_to_account) | **GET** /account/{account}/delegated | Get Delegations to this account
[**get_endorsements_for_account**](AccountApi.md#get_endorsements_for_account) | **GET** /account/{account}/endorsements | Get Endorsements this Account has made
[**get_transaction_for_account_incoming**](AccountApi.md#get_transaction_for_account_incoming) | **GET** /account/{account}/transactions/incoming | Get Transaction
[**get_transaction_for_account_outgoing**](AccountApi.md#get_transaction_for_account_outgoing) | **GET** /account/{account}/transactions/outgoing | Get Transaction


# **get_account**
> Account get_account(account)

Get Account

Get Acccount

### Example 
```python
import time
import tezex_client
from tezex_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = tezex_client.AccountApi()
account = 'account_example' # str | The account

try: 
    # Get Account
    api_response = api_instance.get_account(account)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling AccountApi->get_account: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **str**| The account | 

### Return type

[**Account**](Account.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_account_balance**
> str get_account_balance(account)

Get Account Balance

Get Balance

### Example 
```python
import time
import tezex_client
from tezex_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = tezex_client.AccountApi()
account = 'account_example' # str | The account

try: 
    # Get Account Balance
    api_response = api_instance.get_account_balance(account)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling AccountApi->get_account_balance: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **str**| The account | 

### Return type

**str**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_account_last_seen**
> datetime get_account_last_seen(account)

Get last active date

Get LastSeen Date

### Example 
```python
import time
import tezex_client
from tezex_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = tezex_client.AccountApi()
account = 'account_example' # str | The account

try: 
    # Get last active date
    api_response = api_instance.get_account_last_seen(account)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling AccountApi->get_account_last_seen: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **str**| The account | 

### Return type

**datetime**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_account_operation_count**
> int get_account_operation_count(account)

Get operation count of Account

Get Operation Count

### Example 
```python
import time
import tezex_client
from tezex_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = tezex_client.AccountApi()
account = 'account_example' # str | The account

try: 
    # Get operation count of Account
    api_response = api_instance.get_account_operation_count(account)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling AccountApi->get_account_operation_count: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **str**| The account | 

### Return type

**int**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_account_transaction_count**
> int get_account_transaction_count(account)

Get transaction count of Account

Get Transaction Count

### Example 
```python
import time
import tezex_client
from tezex_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = tezex_client.AccountApi()
account = 'account_example' # str | The account

try: 
    # Get transaction count of Account
    api_response = api_instance.get_account_transaction_count(account)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling AccountApi->get_account_transaction_count: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **str**| The account | 

### Return type

**int**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_delegations_for_account**
> list[Delegation] get_delegations_for_account(account, before=before)

Get Delegations of this account

Get Delegations this Account has made

### Example 
```python
import time
import tezex_client
from tezex_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = tezex_client.AccountApi()
account = 'account_example' # str | The account for which to retrieve Delegations
before = 56 # int | Only Return Delegations before this blocklevel (optional)

try: 
    # Get Delegations of this account
    api_response = api_instance.get_delegations_for_account(account, before=before)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling AccountApi->get_delegations_for_account: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **str**| The account for which to retrieve Delegations | 
 **before** | **int**| Only Return Delegations before this blocklevel | [optional] 

### Return type

[**list[Delegation]**](Delegation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_delegations_to_account**
> list[Delegation] get_delegations_to_account(account, before=before)

Get Delegations to this account

Get that have been made to this Account

### Example 
```python
import time
import tezex_client
from tezex_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = tezex_client.AccountApi()
account = 'account_example' # str | The account to which delegations have been made
before = 56 # int | Only Return Delegations before this blocklevel (optional)

try: 
    # Get Delegations to this account
    api_response = api_instance.get_delegations_to_account(account, before=before)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling AccountApi->get_delegations_to_account: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **str**| The account to which delegations have been made | 
 **before** | **int**| Only Return Delegations before this blocklevel | [optional] 

### Return type

[**list[Delegation]**](Delegation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_endorsements_for_account**
> list[Endorsement] get_endorsements_for_account(account, before=before)

Get Endorsements this Account has made

Get Endorsements this Account has made

### Example 
```python
import time
import tezex_client
from tezex_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = tezex_client.AccountApi()
account = 'account_example' # str | The account for which to retrieve Endorsements
before = 56 # int | Only Return Delegations before this blocklevel (optional)

try: 
    # Get Endorsements this Account has made
    api_response = api_instance.get_endorsements_for_account(account, before=before)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling AccountApi->get_endorsements_for_account: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **str**| The account for which to retrieve Endorsements | 
 **before** | **int**| Only Return Delegations before this blocklevel | [optional] 

### Return type

[**list[Endorsement]**](Endorsement.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_transaction_for_account_incoming**
> Transactions get_transaction_for_account_incoming(account, before=before)

Get Transaction

Get incoming Transactions for a specific Account

### Example 
```python
import time
import tezex_client
from tezex_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = tezex_client.AccountApi()
account = 'account_example' # str | The account for which to retrieve incoming Transactions
before = 56 # int | Only Return transactions before this blocklevel (optional)

try: 
    # Get Transaction
    api_response = api_instance.get_transaction_for_account_incoming(account, before=before)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling AccountApi->get_transaction_for_account_incoming: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **str**| The account for which to retrieve incoming Transactions | 
 **before** | **int**| Only Return transactions before this blocklevel | [optional] 

### Return type

[**Transactions**](Transactions.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_transaction_for_account_outgoing**
> Transactions get_transaction_for_account_outgoing(account, before=before)

Get Transaction

Get outgoing Transactions for a specific Account

### Example 
```python
import time
import tezex_client
from tezex_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = tezex_client.AccountApi()
account = 'account_example' # str | The account for which to retrieve outgoing Transactions
before = 56 # int | Only return transactions before this blocklevel (optional)

try: 
    # Get Transaction
    api_response = api_instance.get_transaction_for_account_outgoing(account, before=before)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling AccountApi->get_transaction_for_account_outgoing: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **str**| The account for which to retrieve outgoing Transactions | 
 **before** | **int**| Only return transactions before this blocklevel | [optional] 

### Return type

[**Transactions**](Transactions.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

