# TransactionOperation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**kind** | **str** |  | [optional] 
**amount** | **int** |  | [optional] 
**destination** | **str** |  | [optional] 
**parameters** | [**TezosScript**](TezosScript.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


