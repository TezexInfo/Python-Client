# TezosScript

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**int** | **str** |  | [optional] 
**string** | **str** |  | [optional] 
**prim** | **str** |  | [optional] 
**args** | [**list[TezosScript]**](TezosScript.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


