# tezex_client.OriginationApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_origination**](OriginationApi.md#get_origination) | **GET** /origination/{origination_hash} | Get Origination


# **get_origination**
> Origination get_origination(origination_hash)

Get Origination

Get a specific Origination

### Example 
```python
import time
import tezex_client
from tezex_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = tezex_client.OriginationApi()
origination_hash = 'origination_hash_example' # str | The hash of the Origination to retrieve

try: 
    # Get Origination
    api_response = api_instance.get_origination(origination_hash)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling OriginationApi->get_origination: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **origination_hash** | **str**| The hash of the Origination to retrieve | 

### Return type

[**Origination**](Origination.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

