# Transaction

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hash** | **str** |  | [optional] 
**branch** | **str** |  | [optional] 
**source** | **str** |  | [optional] 
**public_key** | **str** |  | [optional] 
**level** | **int** |  | [optional] 
**block_hash** | **str** |  | [optional] 
**counter** | **int** |  | [optional] 
**time** | **datetime** |  | [optional] 
**operations** | [**list[TransactionOperation]**](TransactionOperation.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


