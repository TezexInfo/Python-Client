# Account

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | **str** |  | [optional] 
**operation_count** | **int** |  | [optional] 
**sent_transaction_count** | **int** |  | [optional] 
**recv_transaction_count** | **int** |  | [optional] 
**origination_count** | **int** |  | [optional] 
**delegation_count** | **int** |  | [optional] 
**delegated_count** | **int** |  | [optional] 
**endorsement_count** | **int** |  | [optional] 
**first_seen** | **datetime** |  | [optional] 
**last_seen** | **datetime** |  | [optional] 
**name** | **str** |  | [optional] 
**balance** | **str** |  | [optional] 
**total_sent** | **str** |  | [optional] 
**total_received** | **str** |  | [optional] 
**baked_blocks** | **int** |  | [optional] 
**image_url** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


