# coding: utf-8

"""
    TezosAPI

    BETA Tezos API, this may change frequently

    OpenAPI spec version: 0.0.2
    Contact: office@bitfly.at
    Generated by: https://github.com/swagger-api/swagger-codegen.git

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
"""

from pprint import pformat
from six import iteritems
import re


class Account(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    """
    def __init__(self, address=None, operation_count=None, sent_transaction_count=None, recv_transaction_count=None, origination_count=None, delegation_count=None, delegated_count=None, endorsement_count=None, first_seen=None, last_seen=None, name=None, balance=None, total_sent=None, total_received=None, baked_blocks=None, image_url=None):
        """
        Account - a model defined in Swagger

        :param dict swaggerTypes: The key is attribute name
                                  and the value is attribute type.
        :param dict attributeMap: The key is attribute name
                                  and the value is json key in definition.
        """
        self.swagger_types = {
            'address': 'str',
            'operation_count': 'int',
            'sent_transaction_count': 'int',
            'recv_transaction_count': 'int',
            'origination_count': 'int',
            'delegation_count': 'int',
            'delegated_count': 'int',
            'endorsement_count': 'int',
            'first_seen': 'datetime',
            'last_seen': 'datetime',
            'name': 'str',
            'balance': 'str',
            'total_sent': 'str',
            'total_received': 'str',
            'baked_blocks': 'int',
            'image_url': 'str'
        }

        self.attribute_map = {
            'address': 'address',
            'operation_count': 'operation_count',
            'sent_transaction_count': 'sent_transaction_count',
            'recv_transaction_count': 'recv_transaction_count',
            'origination_count': 'origination_count',
            'delegation_count': 'delegation_count',
            'delegated_count': 'delegated_count',
            'endorsement_count': 'endorsement_count',
            'first_seen': 'first_seen',
            'last_seen': 'last_seen',
            'name': 'name',
            'balance': 'balance',
            'total_sent': 'total_sent',
            'total_received': 'total_received',
            'baked_blocks': 'baked_blocks',
            'image_url': 'image_url'
        }

        self._address = address
        self._operation_count = operation_count
        self._sent_transaction_count = sent_transaction_count
        self._recv_transaction_count = recv_transaction_count
        self._origination_count = origination_count
        self._delegation_count = delegation_count
        self._delegated_count = delegated_count
        self._endorsement_count = endorsement_count
        self._first_seen = first_seen
        self._last_seen = last_seen
        self._name = name
        self._balance = balance
        self._total_sent = total_sent
        self._total_received = total_received
        self._baked_blocks = baked_blocks
        self._image_url = image_url

    @property
    def address(self):
        """
        Gets the address of this Account.


        :return: The address of this Account.
        :rtype: str
        """
        return self._address

    @address.setter
    def address(self, address):
        """
        Sets the address of this Account.


        :param address: The address of this Account.
        :type: str
        """

        self._address = address

    @property
    def operation_count(self):
        """
        Gets the operation_count of this Account.


        :return: The operation_count of this Account.
        :rtype: int
        """
        return self._operation_count

    @operation_count.setter
    def operation_count(self, operation_count):
        """
        Sets the operation_count of this Account.


        :param operation_count: The operation_count of this Account.
        :type: int
        """

        self._operation_count = operation_count

    @property
    def sent_transaction_count(self):
        """
        Gets the sent_transaction_count of this Account.


        :return: The sent_transaction_count of this Account.
        :rtype: int
        """
        return self._sent_transaction_count

    @sent_transaction_count.setter
    def sent_transaction_count(self, sent_transaction_count):
        """
        Sets the sent_transaction_count of this Account.


        :param sent_transaction_count: The sent_transaction_count of this Account.
        :type: int
        """

        self._sent_transaction_count = sent_transaction_count

    @property
    def recv_transaction_count(self):
        """
        Gets the recv_transaction_count of this Account.


        :return: The recv_transaction_count of this Account.
        :rtype: int
        """
        return self._recv_transaction_count

    @recv_transaction_count.setter
    def recv_transaction_count(self, recv_transaction_count):
        """
        Sets the recv_transaction_count of this Account.


        :param recv_transaction_count: The recv_transaction_count of this Account.
        :type: int
        """

        self._recv_transaction_count = recv_transaction_count

    @property
    def origination_count(self):
        """
        Gets the origination_count of this Account.


        :return: The origination_count of this Account.
        :rtype: int
        """
        return self._origination_count

    @origination_count.setter
    def origination_count(self, origination_count):
        """
        Sets the origination_count of this Account.


        :param origination_count: The origination_count of this Account.
        :type: int
        """

        self._origination_count = origination_count

    @property
    def delegation_count(self):
        """
        Gets the delegation_count of this Account.


        :return: The delegation_count of this Account.
        :rtype: int
        """
        return self._delegation_count

    @delegation_count.setter
    def delegation_count(self, delegation_count):
        """
        Sets the delegation_count of this Account.


        :param delegation_count: The delegation_count of this Account.
        :type: int
        """

        self._delegation_count = delegation_count

    @property
    def delegated_count(self):
        """
        Gets the delegated_count of this Account.


        :return: The delegated_count of this Account.
        :rtype: int
        """
        return self._delegated_count

    @delegated_count.setter
    def delegated_count(self, delegated_count):
        """
        Sets the delegated_count of this Account.


        :param delegated_count: The delegated_count of this Account.
        :type: int
        """

        self._delegated_count = delegated_count

    @property
    def endorsement_count(self):
        """
        Gets the endorsement_count of this Account.


        :return: The endorsement_count of this Account.
        :rtype: int
        """
        return self._endorsement_count

    @endorsement_count.setter
    def endorsement_count(self, endorsement_count):
        """
        Sets the endorsement_count of this Account.


        :param endorsement_count: The endorsement_count of this Account.
        :type: int
        """

        self._endorsement_count = endorsement_count

    @property
    def first_seen(self):
        """
        Gets the first_seen of this Account.


        :return: The first_seen of this Account.
        :rtype: datetime
        """
        return self._first_seen

    @first_seen.setter
    def first_seen(self, first_seen):
        """
        Sets the first_seen of this Account.


        :param first_seen: The first_seen of this Account.
        :type: datetime
        """

        self._first_seen = first_seen

    @property
    def last_seen(self):
        """
        Gets the last_seen of this Account.


        :return: The last_seen of this Account.
        :rtype: datetime
        """
        return self._last_seen

    @last_seen.setter
    def last_seen(self, last_seen):
        """
        Sets the last_seen of this Account.


        :param last_seen: The last_seen of this Account.
        :type: datetime
        """

        self._last_seen = last_seen

    @property
    def name(self):
        """
        Gets the name of this Account.


        :return: The name of this Account.
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """
        Sets the name of this Account.


        :param name: The name of this Account.
        :type: str
        """

        self._name = name

    @property
    def balance(self):
        """
        Gets the balance of this Account.


        :return: The balance of this Account.
        :rtype: str
        """
        return self._balance

    @balance.setter
    def balance(self, balance):
        """
        Sets the balance of this Account.


        :param balance: The balance of this Account.
        :type: str
        """

        self._balance = balance

    @property
    def total_sent(self):
        """
        Gets the total_sent of this Account.


        :return: The total_sent of this Account.
        :rtype: str
        """
        return self._total_sent

    @total_sent.setter
    def total_sent(self, total_sent):
        """
        Sets the total_sent of this Account.


        :param total_sent: The total_sent of this Account.
        :type: str
        """

        self._total_sent = total_sent

    @property
    def total_received(self):
        """
        Gets the total_received of this Account.


        :return: The total_received of this Account.
        :rtype: str
        """
        return self._total_received

    @total_received.setter
    def total_received(self, total_received):
        """
        Sets the total_received of this Account.


        :param total_received: The total_received of this Account.
        :type: str
        """

        self._total_received = total_received

    @property
    def baked_blocks(self):
        """
        Gets the baked_blocks of this Account.


        :return: The baked_blocks of this Account.
        :rtype: int
        """
        return self._baked_blocks

    @baked_blocks.setter
    def baked_blocks(self, baked_blocks):
        """
        Sets the baked_blocks of this Account.


        :param baked_blocks: The baked_blocks of this Account.
        :type: int
        """

        self._baked_blocks = baked_blocks

    @property
    def image_url(self):
        """
        Gets the image_url of this Account.


        :return: The image_url of this Account.
        :rtype: str
        """
        return self._image_url

    @image_url.setter
    def image_url(self, image_url):
        """
        Sets the image_url of this Account.


        :param image_url: The image_url of this Account.
        :type: str
        """

        self._image_url = image_url

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other
