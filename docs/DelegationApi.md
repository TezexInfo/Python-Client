# tezex_client.DelegationApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_delegation**](DelegationApi.md#get_delegation) | **GET** /delegation/{delegation_hash} | Get Delegation


# **get_delegation**
> Delegation get_delegation(delegation_hash)

Get Delegation

Get a specific Delegation

### Example 
```python
import time
import tezex_client
from tezex_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = tezex_client.DelegationApi()
delegation_hash = 'delegation_hash_example' # str | The hash of the Origination to retrieve

try: 
    # Get Delegation
    api_response = api_instance.get_delegation(delegation_hash)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling DelegationApi->get_delegation: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **delegation_hash** | **str**| The hash of the Origination to retrieve | 

### Return type

[**Delegation**](Delegation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

