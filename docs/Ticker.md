# Ticker

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**btc** | **str** |  | [optional] 
**cny** | **str** |  | [optional] 
**usd** | **str** |  | [optional] 
**eur** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


