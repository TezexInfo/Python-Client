# tezex_client.BlockchainApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**blockheight**](BlockchainApi.md#blockheight) | **GET** /maxLevel | Get Max Blockheight


# **blockheight**
> Level blockheight()

Get Max Blockheight

Get the maximum Level we have seen

### Example 
```python
import time
import tezex_client
from tezex_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = tezex_client.BlockchainApi()

try: 
    # Get Max Blockheight
    api_response = api_instance.blockheight()
    pprint(api_response)
except ApiException as e:
    print "Exception when calling BlockchainApi->blockheight: %s\n" % e
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Level**](Level.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

