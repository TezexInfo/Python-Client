# tezex_client.OperationApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_operation**](OperationApi.md#get_operation) | **GET** /operation/{operation_hash} | Get Operation


# **get_operation**
> Operation get_operation(operation_hash)

Get Operation

Get a specific Operation

### Example 
```python
import time
import tezex_client
from tezex_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = tezex_client.OperationApi()
operation_hash = 'operation_hash_example' # str | The hash of the Operation to retrieve

try: 
    # Get Operation
    api_response = api_instance.get_operation(operation_hash)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling OperationApi->get_operation: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **operation_hash** | **str**| The hash of the Operation to retrieve | 

### Return type

[**Operation**](Operation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

