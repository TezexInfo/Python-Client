# OriginationOperation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**kind** | **str** |  | [optional] 
**manager_pubkey** | **str** |  | [optional] 
**balance** | **int** |  | [optional] 
**spendable** | **bool** |  | [optional] 
**delegateable** | **bool** |  | [optional] 
**delegate** | **str** |  | [optional] 
**script** | [**TezosScript**](TezosScript.md) |  | [optional] 
**storage** | [**TezosScript**](TezosScript.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


