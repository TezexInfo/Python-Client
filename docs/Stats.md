# Stats

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**stat_group** | **str** |  | [optional] 
**stat** | **str** |  | [optional] 
**value** | **str** |  | [optional] 
**start** | **datetime** |  | [optional] 
**end** | **datetime** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


