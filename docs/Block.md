# Block

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hash** | **str** |  | [optional] 
**net_id** | **str** |  | [optional] 
**protocol** | **str** |  | [optional] 
**level** | **int** |  | [optional] 
**proto** | **str** |  | [optional] 
**successors** | [**list[ChainStatus]**](ChainStatus.md) |  | [optional] 
**predecessor** | **str** |  | [optional] 
**time** | **datetime** |  | [optional] 
**validation_pass** | **str** |  | [optional] 
**data** | **str** |  | [optional] 
**chain_status** | **str** |  | [optional] 
**operations_count** | **int** |  | [optional] 
**operations_hash** | **str** |  | [optional] 
**baker** | **str** |  | [optional] 
**seed_nonce_hash** | **str** |  | [optional] 
**proof_of_work_nonce** | **str** |  | [optional] 
**signature** | **str** |  | [optional] 
**priority** | **int** |  | [optional] 
**operation_count** | **int** |  | [optional] 
**total_fee** | **str** |  | [optional] 
**operations** | [**BlockOperationsSorted**](BlockOperationsSorted.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


