# tezex_client
BETA Tezos API, this may change frequently

This Python package is automatically generated by the [Swagger Codegen](https://github.com/swagger-api/swagger-codegen) project:

- API version: 0.0.2
- Package version: 1.0.0
- Build date: 2018-03-06T13:47:15.708+01:00
- Build package: class io.swagger.codegen.languages.PythonClientCodegen

## Requirements.

Python 2.7 and 3.4+

## Installation & Usage
### pip install

If the python package is hosted on Github, you can install directly from Github

```sh
pip install git+https://github.com/GIT_USER_ID/GIT_REPO_ID.git
```
(you may need to run `pip` with root permission: `sudo pip install git+https://github.com/GIT_USER_ID/GIT_REPO_ID.git`)

Then import the package:
```python
import tezex_client 
```

### Setuptools

Install via [Setuptools](http://pypi.python.org/pypi/setuptools).

```sh
python setup.py install --user
```
(or `sudo python setup.py install` to install the package for all users)

Then import the package:
```python
import tezex_client
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```python
import time
import tezex_client
from tezex_client.rest import ApiException
from pprint import pprint
# create an instance of the API class
api_instance = tezex_client.AccountApi
account = 'account_example' # str | The account

try:
    # Get Account
    api_response = api_instance.get_account(account)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling AccountApi->get_account: %s\n" % e

```

## Documentation for API Endpoints

All URIs are relative to *http://betaapi.tezex.info/v2*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*AccountApi* | [**get_account**](docs/AccountApi.md#get_account) | **GET** /account/{account} | Get Account
*AccountApi* | [**get_account_balance**](docs/AccountApi.md#get_account_balance) | **GET** /account/{account}/balance | Get Account Balance
*AccountApi* | [**get_account_last_seen**](docs/AccountApi.md#get_account_last_seen) | **GET** /account/{account}/last_seen | Get last active date
*AccountApi* | [**get_account_operation_count**](docs/AccountApi.md#get_account_operation_count) | **GET** /account/{account}/operations_count | Get operation count of Account
*AccountApi* | [**get_account_transaction_count**](docs/AccountApi.md#get_account_transaction_count) | **GET** /account/{account}/transaction_count | Get transaction count of Account
*AccountApi* | [**get_delegations_for_account**](docs/AccountApi.md#get_delegations_for_account) | **GET** /account/{account}/delegations | Get Delegations of this account
*AccountApi* | [**get_delegations_to_account**](docs/AccountApi.md#get_delegations_to_account) | **GET** /account/{account}/delegated | Get Delegations to this account
*AccountApi* | [**get_endorsements_for_account**](docs/AccountApi.md#get_endorsements_for_account) | **GET** /account/{account}/endorsements | Get Endorsements this Account has made
*AccountApi* | [**get_transaction_for_account_incoming**](docs/AccountApi.md#get_transaction_for_account_incoming) | **GET** /account/{account}/transactions/incoming | Get Transaction
*AccountApi* | [**get_transaction_for_account_outgoing**](docs/AccountApi.md#get_transaction_for_account_outgoing) | **GET** /account/{account}/transactions/outgoing | Get Transaction
*BlockApi* | [**blocks_all**](docs/BlockApi.md#blocks_all) | **GET** /blocks/all | Get All Blocks 
*BlockApi* | [**blocks_by_level**](docs/BlockApi.md#blocks_by_level) | **GET** /blocks/{level} | Get All Blocks for a specific Level
*BlockApi* | [**blocks_by_level_range**](docs/BlockApi.md#blocks_by_level_range) | **GET** /blocks/{startlevel}/{stoplevel} | Get All Blocks for a specific Level-Range
*BlockApi* | [**get_block**](docs/BlockApi.md#get_block) | **GET** /block/{blockhash} | Get Block By Blockhash
*BlockApi* | [**get_block_delegations**](docs/BlockApi.md#get_block_delegations) | **GET** /block/{blockhash}/operations/delegations | Get Delegations of a Block
*BlockApi* | [**get_block_endorsements**](docs/BlockApi.md#get_block_endorsements) | **GET** /block/{blockhash}/operations/endorsements | Get Endorsements of a Block
*BlockApi* | [**get_block_operations_sorted**](docs/BlockApi.md#get_block_operations_sorted) | **GET** /block/{blockhash}/operations | Get operations of a block, sorted
*BlockApi* | [**get_block_originations**](docs/BlockApi.md#get_block_originations) | **GET** /block/{blockhash}/operations/originations | Get Originations of a Block
*BlockApi* | [**get_block_transaction**](docs/BlockApi.md#get_block_transaction) | **GET** /block/{blockhash}/operations/transactions | Get Transactions of Block
*BlockApi* | [**recent_blocks**](docs/BlockApi.md#recent_blocks) | **GET** /blocks/recent | returns the last 25 blocks
*BlockchainApi* | [**blockheight**](docs/BlockchainApi.md#blockheight) | **GET** /maxLevel | Get Max Blockheight
*DelegationApi* | [**get_delegation**](docs/DelegationApi.md#get_delegation) | **GET** /delegation/{delegation_hash} | Get Delegation
*EndorsementApi* | [**get_endorsement**](docs/EndorsementApi.md#get_endorsement) | **GET** /endorsement/{endorsement_hash} | Get Endorsement
*EndorsementApi* | [**get_endorsement_for_block**](docs/EndorsementApi.md#get_endorsement_for_block) | **GET** /endorsement/for/{block_hash} | Get Endorsement
*MarketApi* | [**candlestick**](docs/MarketApi.md#candlestick) | **GET** /price/{denominator}/{numerator}/{period} | Candlestick Data
*MarketApi* | [**ticker**](docs/MarketApi.md#ticker) | **GET** /ticker/{numerator} | Get Ticker for a specific Currency
*NetworkApi* | [**network**](docs/NetworkApi.md#network) | **GET** /network | Get Network Information
*OperationApi* | [**get_operation**](docs/OperationApi.md#get_operation) | **GET** /operation/{operation_hash} | Get Operation
*OriginationApi* | [**get_origination**](docs/OriginationApi.md#get_origination) | **GET** /origination/{origination_hash} | Get Origination
*StatsApi* | [**get_statistics**](docs/StatsApi.md#get_statistics) | **GET** /stats/{group}/{stat}/{period} | Get Statistics
*StatsApi* | [**get_stats_overview**](docs/StatsApi.md#get_stats_overview) | **GET** /stats/overview | Returns some basic Info
*TransactionApi* | [**get_transaction**](docs/TransactionApi.md#get_transaction) | **GET** /transaction/{transaction_hash} | Get Transaction
*TransactionApi* | [**get_transactions_recent**](docs/TransactionApi.md#get_transactions_recent) | **GET** /transactions/recent | Returns the last 50 Transactions
*TransactionApi* | [**transactions_all**](docs/TransactionApi.md#transactions_all) | **GET** /transactions/all | Get All Transactions
*TransactionApi* | [**transactions_by_level_range**](docs/TransactionApi.md#transactions_by_level_range) | **GET** /transactions/{startlevel}/{stoplevel} | Get All Transactions for a specific Level-Range


## Documentation For Models

 - [Account](docs/Account.md)
 - [Block](docs/Block.md)
 - [BlockOperationsSorted](docs/BlockOperationsSorted.md)
 - [BlockRange](docs/BlockRange.md)
 - [BlocksAll](docs/BlocksAll.md)
 - [Candlestick](docs/Candlestick.md)
 - [ChainStatus](docs/ChainStatus.md)
 - [Delegation](docs/Delegation.md)
 - [Endorsement](docs/Endorsement.md)
 - [Level](docs/Level.md)
 - [NetworkInfo](docs/NetworkInfo.md)
 - [Operation](docs/Operation.md)
 - [Origination](docs/Origination.md)
 - [OriginationOperation](docs/OriginationOperation.md)
 - [Stats](docs/Stats.md)
 - [StatsOverview](docs/StatsOverview.md)
 - [TezosScript](docs/TezosScript.md)
 - [Ticker](docs/Ticker.md)
 - [Transaction](docs/Transaction.md)
 - [TransactionOperation](docs/TransactionOperation.md)
 - [TransactionRange](docs/TransactionRange.md)
 - [Transactions](docs/Transactions.md)


## Documentation For Authorization

 All endpoints do not require authorization.


## Author

office@bitfly.at

